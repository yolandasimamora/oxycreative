@extends('layouts.app')

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Books</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('books.create') }}">Add Books</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Details</th>
                <th>Authors</th>
                <th width="280px">Actions</th>
            </tr>
            @foreach ($books ?? '' as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->detail }}</td>
                    <td>
                        <ul>
                            @foreach ($book->authors as $auth)
                                <li>{{ $auth->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <form action="{{ route('books.destroy',$book->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('books.show',$book->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('books.edit',$book->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')
                            @if ($is_admin == 1)
                                <button type="submit" class="btn btn-danger">Delete</button>
                            @endif
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    {!! $books ?? ''->links() !!}

@endsection