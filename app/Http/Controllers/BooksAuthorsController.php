<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Anggota;
use App\Model\Hadiah;

class BookAuthorsController extends Controller
{
    public function index()
    {
    	$anggota = Anggota::get();
    	return view('anggota', ['anggota' => $anggota]);
    }
}
