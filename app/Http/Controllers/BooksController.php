<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Authors;
use App\Models\AuthorsBooks;
use Illuminate\Http\Request;
use App\Event;
use Session;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $books = Books::latest()->with('authors')->paginate(10);
        if(!is_null($request->session()->get('email'))){
        $email = $request->session()->get('email');
                 $collection = DB::table('users')->where('email', $email)->get();
                 $result = $collection->toArray();  
                 $is_admin = json_encode($result[0]->is_admin);
        } else {
            $is_admin = 0;
        }
        return view('admin.books.index',compact('books', 'is_admin'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $authors = Authors::pluck('name', 'id');
        return view('admin.books.create')->with('authors', $authors);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
            'title' => 'required|max:255|string',
            'detail'=> 'required|text',
    	]);
    	$save_book = Books::create([
            'title' =>  $request->title,
            'detail' =>  $request->detail,
        ]);
        $insertedId = $save_book->id;

        if (!is_null($request->authors_id)) {
            $authors_id = $request->authors_id;
            foreach($authors_id as $auth_id){
                $save = AuthorsBooks::create([
                    'books_id' => $insertedId,
                    'authors_id' => $auth_id 
                ]);
            }
        }

        return redirect()->route('books.index')
            ->with('success','Books updated successfully');
        
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $books = Books::find($id);
        return view('admin.books.show', compact('books'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $books = Books::find($id);
        $authors = Authors::pluck('name', 'id');
        $collection = Arr::flatten(DB::table('authors_books')->select('authors_id')->where('books_id', $id)->get()->pluck('authors_id'));
        //var_dump($collection); die();
        return view('admin.books.edit', compact('books', 'authors', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, Request $request)
    {
        DB::table('books')->where('id',$id)->update(array(
            'title' =>  $request->title,
            'detail' =>  $request->detail,
        )); 
        if(!is_null($request->authors_id)){
            $check = Arr::flatten(DB::table('authors_books')->select('authors_id')->where('books_id', $id)->get()->pluck('authors_id'));
            $existed_data = count($check);
            $inserted_data = count($request->authors_id);
            $differenceArray = array_diff($check, $request->authors_id);
            if($inserted_data < $existed_data){
                foreach($differenceArray as $auth_id) {
                    $delete_id = DB::table('authors_books')->select('id')->where('authors_id',$auth_id)->where('books_id', $id)->first(); 
                    $delete = DB::table('authors_books')->where('id',$delete_id->id)->delete();
                }
            } else {
                if (!is_null($request->authors_id)) {
                    $authors_id = $request->authors_id;
                    foreach($authors_id as $auth_id){
                        $check = AuthorsBooks::where('authors_id', '=', $auth_id)->where('books_id', $id)->first();
                        if ($check === null) {
                            $save = AuthorsBooks::create([
                                'books_id' => $id,
                                'authors_id' => $auth_id 
                            ]);
                        }
                    }
                }
            }
        }
        return redirect()->route('books.index')
            ->with('success','Books updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('books')
            ->where('id',$id)
            ->delete();
        return redirect()->route('books.index')
            ->with('success','Books deleted successfully');
    }
}
