<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class Users extends Authenticatable
{
	protected $fillable = ['name', 'email', 'password'];

}
