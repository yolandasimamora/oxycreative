<?php

use Illuminate\Database\Seeder;

class AuthorsBooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(database_path() . '/seeds/author_book.sql');
         DB::statement($sql);
    }
}
