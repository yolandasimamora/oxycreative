$('.latest-news-slider').slick({
    rows: 2,
    slidesToShow: 3,
    slidesToScroll: 3, 
    dots: true,   
    responsive: [ 
        {
            breakpoint: 991,
            settings: {  
                slidesToShow: 2,
                slidesToScroll: 2, 
            }
          } ,
      {
        breakpoint: 767,
        settings: {  
            slidesToShow: 1,
            slidesToScroll: 1, 
            arrows:false
        }
      } 
    ]   
  });
  $('.client-container').slick({ 
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,   
    arrows:false,
    responsive: [ 
        {
            breakpoint: 991,
            settings: {  
                slidesToShow: 4,
                slidesToScroll: 1, 
            }
          } ,
      {
        breakpoint: 767,
        settings: {  
            slidesToShow: 2,
            slidesToScroll: 1,
        }
      } 
    ]   
  });
