// resources/assets/js/components/App.js

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'

class Index extends Component {
    render () {
        return (
          <BrowserRouter>
            <div>
              <Header />
            </div>
          </BrowserRouter>
        )
    }
}

ReactDOM.render(<Index />, document.getElementById('index'))