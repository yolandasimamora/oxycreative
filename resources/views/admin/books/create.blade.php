@extends('layouts.app')

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Add Books</h3>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('books.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Title:</strong>
                    <input type="text" name="title" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea name="detail" class="form-control"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <select class="form-control select2-multi" name="authors_id[]" multiple="multiple" required="required">
                  @foreach ($authors as $key => $value)
                    <option value="{{ $key }}"> 
                        {{ $value }} 
                    </option>
                  @endforeach    
                </select>
            </div>
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="margin-top: 20px;">
                <button type="submit" class="btn btn-success">Add Books</button>
            </div>
        </div>

    </form>
<script>
    $('.select2-multi').select2();
</script>
@endsection