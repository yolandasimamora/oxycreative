@extends('admin.layouts.layout')

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Users</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('users.create') }}">Add User</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Description</th>
                <th width="280px">Actions</th>
            </tr>
            @foreach ($user ?? '' as $usr)
                <tr>
                    <td>{{ $usr->id }}</td>
                    <td>{{ $usr->name }}</td>
                    <td>{{ $usr->email }}</td>
                    <td>{{ $usr->description }}</td>
                    <td>
                        <form action="{{ route('users.destroy',$usr->id) }}" method="POST">

                            <a class="btn btn-info" href="{{ route('users.show',$usr->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('users.edit',$usr->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    {!! $user ?? ''->links() !!}

@endsection