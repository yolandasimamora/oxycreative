INSERT INTO `authors` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
		(1, 'Robert Cecil Martin', 'Book Author', '2021-02-01 07:53:57', '2021-02-01 07:53:57'),
		(2, 'R.H. Sianipar', 'Book Author', '2021-02-01 07:55:14', '2021-02-01 07:55:14'),
		(3, 'Ronald L. Rivest', 'Book author', '2021-02-01 07:56:00', '2021-02-01 07:56:00'),
		(4, 'Marijn Haverbeke', 'Book Author', '2021-02-01 07:56:19', '2021-02-01 07:56:19'),
		(5, 'Donald Knuth', 'Book Author', '2021-02-01 07:56:34', '2021-02-01 07:56:34'),
		(6, 'Abdul Kadir', 'Book Author', '2021-02-01 07:56:52', '2021-02-01 07:56:52'),
		(7, 'David A. Black', 'Book Author', '2021-02-01 07:57:17', '2021-02-01 07:57:17'),
		(8, 'Steve Oualline', 'Book author', '2021-02-01 07:57:36', '2021-02-01 07:57:36'),
		(9, 'Brian Goetz', 'Book Author', '2021-02-01 07:57:52', '2021-02-01 07:57:52'),
		(10, 'Dawn Griffiths', 'Book Author', '2021-02-01 07:58:23', '2021-02-01 07:58:23'),
		(11, 'Davod Griffiths', 'Book Author', '2021-02-01 07:58:32', '2021-02-01 07:58:32');