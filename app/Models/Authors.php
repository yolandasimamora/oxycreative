<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Books;

class Authors extends Model
{
    protected $fillable = ['name', 'detail'];

    public function books()
    {
    	return $this->belongsToMany('App\Models\Books', 'authors_books');
    }
}
