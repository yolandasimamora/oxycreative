@extends('layouts.app')

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Authors</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('authors.create') }}">Add Authors</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Details</th>
                <th width="280px">Actions</th>
            </tr>
            @foreach ($authors ?? '' as $auth)
                <tr>
                    <td>{{ $auth->id }}</td>
                    <td>{{ $auth->name }}</td>
                    <td>{{ $auth->detail }}</td>
                    <td>
                        <form action="{{ route('authors.destroy',$auth->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('authors.show',$auth->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('authors.edit',$auth->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')
                            @if ($is_admin == 1)
                                <button type="submit" class="btn btn-danger">Delete</button>
                            @endif
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    {!! $authors ?? ''->links() !!}

@endsection