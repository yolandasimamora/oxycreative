<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$sql = file_get_contents(database_path() . '/seeds/author.sql');
         DB::statement($sql);
    }
}
