<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//News Index Admin
Route::resource('books', 'BooksController')->middleware('auth');
Route::resource('authors', 'AuthorsController')->middleware('auth');
Route::resource('users',UsersController::class)->middleware('auth');
Auth::routes();
Route::get('/admin', function () {
    return view('admin');
});
Route::get('/', function () {
    return view('admin');
});
Route::get('books/{id}', 'BooksController@show');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
