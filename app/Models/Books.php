<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Authors;

class Books extends Model
{
    protected $fillable = ['title', 'detail'];

    public function authors()
    {
    	return $this->belongsToMany('App\Models\Authors', 'authors_books');
    }
}
