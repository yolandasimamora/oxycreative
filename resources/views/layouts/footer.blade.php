<footer class="site_footer">
	<div class="container">
		 <div class="row">
			 <div class="col-sm-8">
				<div class="footer-menu">
					<div class="row">
						<div class="col-sm-4">
							<div class="menu-title">About</div>
							<ul class="footermenu-list">
								<li class="footermenu-item"><a href="" class="menu-link">Home</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">About</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Contact</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Social Media</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Blog</a></li>
							</ul>
						</div>
						<div class="col-sm-4">
							<div class="menu-title">Works</div>
							<ul class="footermenu-list">
								<li class="footermenu-item"><a href="" class="menu-link">Forest Solutions</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Soy Scorecards</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Palm Oil Scorecards</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">School of Fist</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">RSPO</a></li>
							</ul>
						</div>
						<div class="col-sm-4">
							<div class="menu-title">Clients</div>
							<ul class="footermenu-list">
								<li class="footermenu-item"> <a href="" class="menu-link">WWF International</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">RSPO</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Lorem p[sum</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Dolor sit</a></li>
								<li class="footermenu-item"><a href="" class="menu-link">Amet.co</a></li>
							</ul>
						</div>
					</div>
				</div>
			 </div>
			 <div class="col-sm-4">
				 <div class="footer-info">
					 <div class="footerinfo-title">Contact US</div>
					 <p>Ut tristique non elit nec accumsan. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imperdiet varius arcu quis faucibus. Sed at consectetur lorem.</p>
					 <div class="footer-contact">
						 <form action="" class="contactfooter">
						 <div class="form-group">
						 <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"  placeholder="Message"></textarea>
							 
						</div>
						<div class="form-group group2-input">
							 
							 <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address">
							 <button type="submit" class="btn btn-primary">Submit</button>
						 
							 
						</div> 
						
						 </form>
					 </div>
				 </div>
			 </div>
		 </div>
	</div>
</footer> 