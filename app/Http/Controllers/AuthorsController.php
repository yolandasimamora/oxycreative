<?php

namespace App\Http\Controllers;

use App\Models\Authors;
use App\Models\Books;
use Illuminate\Http\Request;
use App\Event;
use Session;
use Auth;
use Illuminate\Support\Facades\DB;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $authors = Authors::latest()->paginate(10);
        if(!is_null($request->session()->get('email'))){
        $email = $request->session()->get('email');
                 $collection = DB::table('users')->where('email', $email)->get();
                 $result = $collection->toArray();  
                 $is_admin = json_encode($result[0]->is_admin);
        } else {
            $is_admin = 0;
        }
        return view('admin.authors.index',compact('authors', 'is_admin'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

    	$save = Authors::create([
            'name' =>  $request->name,
            'detail' =>  $request->detail,
        ]);

        return redirect()->route('authors.index')
            ->with('success','Authors updated successfully');
        
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $authors = Authors::find($id);
        return view('admin.authors.show', compact('authors'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $authors = Authors::find($id);
        return view('admin.authors.edit', compact('authors', 'books'));
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        DB::table('authors')->where('id',$id)->update(array(
            'name' =>  $request->name,
            'detail' =>  $request->detail,
        )); 
        return redirect()->route('authors.index')
            ->with('success','Authors updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('authors')->where('id',$id)
            ->where('id',$id)
            ->delete();
        return redirect()->route('authors.index')
            ->with('success','Authors deleted successfully');
    }
}